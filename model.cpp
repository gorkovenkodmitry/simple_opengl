#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <fstream>
#include <vector>
#include "model.h"

Model::Model(const char *filename) : verts_(), uvw_(), faces_(), textures_() {
    std::ifstream in;
    in.open (filename, std::ifstream::in);
    if (in.fail()) return;
    std::string line;
    while (!in.eof()) {
        std::getline(in, line);
        std::istringstream iss(line.c_str());
        char trash;
        if (!line.compare(0, 2, "v ")) {
            iss >> trash;
            Vec3f v;
            for (int i=0;i<3;i++) iss >> v[i];
            verts_.push_back(v);
        } else if (!line.compare(0, 3, "vt ")) {
            iss >> trash >> trash;
            Vec3f uv;
            for (int i=0;i<3;i++) iss >> uv[i];
            uvw_.push_back(uv);
        }  else if (!line.compare(0, 2, "f ")) {
            Vec3i f;
            Vec3i t;
            Vec3i tmp;
            iss >> trash;
            // std::cout << line << "\n";
            int i=0;
            while (iss >> tmp[0] >> trash >> tmp[1] >> trash >> tmp[2]) {
                f[i]=tmp[0]-1;
                t[i]=tmp[1]-1;
                i++;
            }
            // std::cout << f;
            // std::cout << t;
            faces_.push_back(f);
            textures_.push_back(t);
        }
    }
    std::cerr << "# v# " << verts_.size() << " f# "  << faces_.size() << " vt# " << uvw_.size() << std::endl;
    load_texture(filename, "_diffuse.tga", diffusemap_);


    // std::string line, val;
    // std::ifstream model_file(filename);
    // if (!model_file.is_open()) {
    //     return;
    // }
    // int space_ind = -1;
    // while (std::getline(model_file, line)) {
    //     if (line.substr(0, 2).compare("v ") == 0) {
    //         Vec3f vert;
    //         line = line.substr(2, line.length());
    //         for (int i=0; i<3; i++) {
    //             space_ind = (line.find(" ") >= 0) ? line.find(" ") : line.length();
    //             vert[i] = (float) std::atof(line.substr(0, space_ind).c_str());
    //             line = line.substr(space_ind + 1, line.length());
    //         }
    //         verts_.push_back(vert);
    //     } else if (line.substr(0, 3).compare("vt ") == 0) {
    //         Vec3f uvw;
    //         std::string x = line;
    //         // std::cout << x << "\n";
    //         std::string y = "vt  ";
    //         line = line.substr(4, line.length());
    //         for (int i=0; i<3; i++) {
    //             space_ind = (line.find(" ") >= 0) ? line.find(" ") : line.length();
    //             uvw[i] = (float) std::atof(line.substr(0, space_ind).c_str());
    //             line = line.substr(space_ind + 1, line.length());
    //             y += uvw[i];
    //         }
    //         // std::cout << y << "\n";
    //         uvw_.push_back(uvw);
    //     } else if (line.substr(0, 2).compare("f ") == 0) {
    //         Vec3i f;
    //         Vec3i t;
    //         line = line.substr(2, line.length());
    //         for (int i=0; i<3; i++) {
    //             space_ind = (line.find(" ") >= 0)? line.find(" ") : line.length();
    //             val = line.substr(0, line.find(" "));
    //             f[i] = (int) std::atoi(val.substr(0, line.find("/")).c_str()) - 1;
    //             val = val.substr(line.find("/")+1, val.length());
    //             t[i] = (int) std::atoi(val.substr(0, line.find("/")).c_str()) - 1;
    //             line = line.substr(space_ind + 1, line.length());
    //         }
    //         faces_.push_back(f);
    //         textures_.push_back(t);
    //     }
    // }
    // model_file.close();
    // std::cout << "# v# " << verts_.size() << " f# "  << faces_.size() << " vt# " << uvw_.size() <<  std::endl;
    // load_texture(filename, "_diffuse.tga", diffusemap_);
}

Model::~Model() {
}

int Model::nverts() {
    return (int)verts_.size();
}

int Model::nfaces() {
    return (int)faces_.size();
}

int Model::nuvw() {
    return (int)uvw_.size();
}

Vec3i Model::face(int idx) {
    return faces_[idx];
}

Vec3i Model::texture(int idx) {
    return textures_[idx];
}

Vec3f Model::vert(int i) {
    return verts_[i];
}

Vec3i Model::uvw(int i) {
    return Vec3i(uvw_[i].x*diffusemap_.get_width(), uvw_[i].y*diffusemap_.get_height(), uvw_[i].z);
}

void Model::load_texture(std::string filename, const char *suffix, TGAImage &img) {
    std::string texfile(filename);
    size_t dot = texfile.find_last_of(".");
    if (dot!=std::string::npos) {
        texfile = texfile.substr(0,dot) + std::string(suffix);
        std::cerr << "texture file " << texfile << " loading " << (img.read_tga_file(texfile.c_str()) ? "ok" : "failed") << std::endl;
        img.flip_vertically();
    }
}
TGAColor Model::diffuse(Vec2i uv) {
    if (uv.x<0 || uv.x>diffusemap_.get_width() || uv.y<0 || uv.y>diffusemap_.get_height()) {
        return TGAColor(0, 0, 0, 255);
    }
    return diffusemap_.get(uv.x, uv.y);
}
