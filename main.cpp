#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <limits>
#include "tgaimage.h"
#include "geometry.h"
#include "model.h"

TGAColor white = TGAColor(255, 255, 255, 255);
TGAColor red   = TGAColor(255, 0,   0,   255);
TGAColor green = TGAColor(0,   255, 0,   255);
TGAColor blue  = TGAColor(0,   0,   255, 255);
const int width = 1024;
const int height = 1024;
Model *model = NULL;
Vec3f light_dir(0,0,-1);

void line(int x0, int y0, int x1, int y1, TGAImage &image, TGAColor color) 
{
    bool steep = false;
    if (std::abs(x0-x1)<std::abs(y0-y1)) {
        std::swap(x0, y0);
        std::swap(x1, y1);
        steep = true;
    }
    if (x0>x1) {
        std::swap(x0, x1);
        std::swap(y0, y1);
    }
    int dx = x1-x0;
    int dy = y1-y0;
    int derror2 = std::abs(dy)*2;
    int error2 = 0;
    int y = y0;
    for (int x=x0; x<=x1; x++) {
        if (steep) {
            image.set(y, x, color);
        } else {
            image.set(x, y, color);
        }
        error2 += derror2;

        if (error2 > dx) {
            y += (y1>y0?1:-1);
            error2 -= dx*2;
        }
    }
}

void line(Vec2i &t0, Vec2i &t1, TGAImage &image, TGAColor color)
{
    line(t0.x, t0.y, t1.x, t1.y, image, color);
}   

void triangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage &image, TGAColor color) 
{
    if (t0.y == t1.y && t0.y == t2.y) return;
    // отсортируем вершины
    if (t0.y > t1.y) std::swap(t0, t1);
    if (t1.y > t2.y) std::swap(t1, t2);
    if (t0.y > t1.y) std::swap(t0, t1);

    int total_height = t2.y - t0.y;
    for (int y=t0.y; y<=t2.y; y++) {
        bool second_half = y>t1.y;
        int segment_height = (second_half)? t2.y-t1.y+1:t1.y-t0.y+1;
        if (segment_height==0) continue;
        float alpha = (float)(y-t0.y)/total_height;
        float beta  = (second_half)?(float)(y-t1.y)/segment_height:(float)(y-t0.y)/segment_height;
        Vec2i A = t0 + (t2-t0)*alpha;
        Vec2i B = (second_half)?t1 + (t2-t1)*beta:t0 + (t1-t0)*beta;
        if (A.x>B.x) std::swap(A, B);
        for (int x=A.x; x<=B.x; x++) {
            image.set(x, y, color);
        }
    }
} 

void triangle(Vec3i t0, Vec3i t1, Vec3i t2, TGAImage &image, TGAColor color, int *zbuffer) 
{
    if (t0.y == t1.y && t0.y == t2.y) return;
    // отсортируем вершины
    if (t0.y < t1.y) std::swap(t0, t1);
    if (t1.y < t2.y) std::swap(t1, t2);
    if (t0.y < t1.y) std::swap(t0, t1);

    float dx = t1.x - t0.x;
    float dy = t1.y - t0.y;
    float dxdy1, dxdy2, dzdx;
    int x0, x1, x, y, ind, z, z0, z1;
    y = t0.y;
    x = t0.x;
    z0 = t0.z;
    z1 = t1.z;
    dzdx = (dx != 0) ? (z1 - z0) / dx : 0;
    dxdy1 = (dy != 0) ? dx / dy : 0;
    dx = t2.x - t0.x;
    dy = t2.y - t0.y;
    dxdy2 = (dy != 0) ? dx / dy : 0;
    for (int i = t0.y; i >= t2.y; i--) {
        x0 = (float)(i - y) * dxdy1 + x;
        x1 = (float)(i - t0.y) * dxdy2 + t0.x;
        if (x0 > x1) {
            std::swap(x0, x1);
        }
        for (int j=x0; j<=x1; j++) {
            ind = j + i*width;
            z = (x0 == x1) ? 1. : (float) (j - x0) * dzdx + z0;
            if (zbuffer[ind] < z) {
                zbuffer[ind] = z;
                image.set(j, i, color);
            }
        }
        if (i == t1.y) {
            dx = t2.x - t1.x;
            dy = t2.y - t1.y;
            x = t1.x;
            y = t1.y;
            z0 = t1.z;
            z1 = t2.z;
            dzdx = (dx != 0) ? (z1 - z0) / dx : 0;
            dxdy1 = (dy != 0) ? dx / dy : 0;
        }
    }
} 

void triangle_old(Vec3i t0, Vec3i t1, Vec3i t2, Vec3i uvw0, Vec3i uvw1, Vec3i uvw2, TGAImage &image, int *zbuffer, Model &model, float intensity) 
{
    if (t0.y == t1.y && t0.y == t2.y) return;
    // отсортируем вершины
    if (t0.y > t1.y) {std::swap(t0, t1); std::swap(uvw0, uvw1);}
    if (t1.y > t2.y) {std::swap(t1, t2); std::swap(uvw1, uvw2);}
    if (t0.y > t1.y) {std::swap(t0, t1); std::swap(uvw0, uvw1);}

    float dx = t1.x - t0.x;
    float dy = t1.y - t0.y;
    float dxdy1, dxdy2, dzdx;
    int x0, x1, x, y, ind, z, z0, z1;
    y = t0.y;
    x = t0.x;
    z0 = t0.z;
    z1 = t1.z;
    dzdx = (dx != 0) ? (z1 - z0) / dx : 0;
    dxdy1 = (dy != 0) ? dx / dy : 0;
    dx = t2.x - t0.x;
    dy = t2.y - t0.y;
    dxdy2 = (dy != 0) ? dx / dy : 0;
    Vec3i e1, e2, j1, j2, n, k, t;
    e1 = t0 - t1;
    e2 = t2 - t1;
    j1 = uvw0 - uvw1;
    j2 = uvw2 - uvw1;
    // матрица перехода
    float params[5];
    params[0] = 1.0 / (e1.x * e2.y - e2.x * e1.y);
    params[1] = (j1.x * e2.y - e1.y * j2.x) * params[0];
    params[2] = (j1.y * e2.y - j2.y * e1.y) * params[0];
    params[3] = (j2.x * e1.x - j1.x * e2.x) * params[0];
    params[4] = (j2.y * e1.x - j1.y * e2.x) * params[0];
    for (int i = t0.y; i <= t2.y; i++) {
        x0 = (float)(i - y) * dxdy1 + x;
        x1 = (float)(i - t0.y) * dxdy2 + t0.x;
        if (x0 > x1) {
            std::swap(x0, x1);
        }
        for (int j=x0; j<=x1; j++) {
            ind = j + i*width;
            z = (x0 == x1) ? 1. : (float) (j - x0) * dzdx + z0;
            if (zbuffer[ind] < z) {
                zbuffer[ind] = z;
                n = Vec3i(j, i, z) - t1;
                t.x = n.x * params[1] + n.y * params[2];
                t.y = n.x * params[3] + n.y * params[4];
                t = t + uvw1;
                image.set(j, i, model.diffuse(Vec2i(t.x, t.y)));
                // TGAColor c = model.diffuse(Vec2i(t.x, t.y));
                // image.set(j, i, TGAColor(c.r*intensity, c.g*intensity, c.b*intensity, 255));
            }
        }
        if (i == t1.y) {
            dx = t2.x - t1.x;
            dy = t2.y - t1.y;
            x = t1.x;
            y = t1.y;
            z0 = t1.z;
            z1 = t2.z;
            dzdx = (dx != 0) ? (z1 - z0) / dx : 0;
            dxdy1 = (dy != 0) ? dx / dy : 0;
        }
    }
}

void triangle(Vec3i t0, Vec3i t1, Vec3i t2, Vec3i uv0, Vec3i uv1, Vec3i uv2, TGAImage &image, int *zbuffer, Model &model, float intensity)
{
    if (t0.y == t1.y && t0.y == t2.y) return;
    // отсортируем вершины
    if (t0.y > t1.y) {std::swap(t0, t1);std::swap(uv0, uv1);}
    if (t1.y > t2.y) {std::swap(t1, t2);std::swap(uv1, uv2);}
    if (t0.y > t1.y) {std::swap(t0, t1);std::swap(uv0, uv1);}

    int total_height = t2.y - t0.y;
    for (int i=0; i<total_height; i++) {
        bool second_half = i>t1.y-t0.y || t1.y==t0.y;
        int segment_height = second_half ? t2.y-t1.y : t1.y-t0.y;
        if (segment_height==0) continue;
        float alpha = (float)i/total_height;
        float beta  = (float)(i-(second_half ? t1.y-t0.y : 0))/segment_height;
        Vec3i A =               t0 + Vec3f(t2-t0)*alpha;
        Vec3i B = second_half ? t1 + Vec3f(t2-t1)*beta : t0 + Vec3f(t1-t0)*beta;
        Vec3i uvA =               uv0 + Vec3f(uv2-uv0)*alpha;
        Vec3i uvB = second_half ? uv1 + Vec3f(uv2-uv1)*beta : uv0 + Vec3f(uv1-uv0)*beta;
        if (A.x>B.x) std::swap(A, B);
        for (int j=A.x; j<=B.x; j++) {
            float phi = B.x==A.x ? 1. : (float)(j-A.x)/(float)(B.x-A.x);
            Vec3i P = Vec3f(A) + Vec3f(B-A)*phi;
            Vec3i uvP = Vec3f(uvA) + Vec3f(uvB-uvA)*phi;
            int idx = P.x+P.y*width;
            if (zbuffer[idx]<P.z) {
                zbuffer[idx] = P.z;
                TGAColor color = model.diffuse(Vec2i(uvP.x, uvP.y));
                image.set(P.x, P.y, TGAColor(color.r*intensity,color.g*intensity,color.b*intensity,255));
            }
        }
    }
}

void rasterize(Vec2i p0, Vec2i p1, TGAImage &image, TGAColor color, int ybuffer[]) {
    if (p0.x>p1.x) {
        std::swap(p0, p1);
    }
    for (int x=p0.x; x<=p1.x; x++) {
        float t = (x-p0.x)/(float)(p1.x-p0.x);
        int y = p0.y*(1.-t) + p1.y*t;
        if (ybuffer[x]<y) {
            ybuffer[x] = y;
            for (int j=0; j<16; j++) {
                image.set(x, j, color);
            }
        }
    }
}

int main(int argc, char** argv)
{
    if (2==argc) {
        model = new Model(argv[1]);
    } else {
        model = new Model("obj/african_head.obj");
    }
    
    int *zbuffer = new int[width * height];
    for (int i=0; i<(width * height); i++) {
        zbuffer[i] = std::numeric_limits<int>::min();
    }
    TGAImage image(width, height, TGAImage::RGB);
    for (int i=0; i<model->nfaces(); i++) {
        Vec3i face = model->face(i);
        Vec3i texture = model->texture(i);
        Vec3i screen_coords[3];
        Vec3f world_coords[3];
        Vec3i texture_coords[3];
        for (int j=0; j<3; j++) {
            Vec3f v = model->vert(face[j]);
            Vec3i uvw = model->uvw(texture[j]);
            screen_coords[j] = Vec3i((v.x+1.)*width/2., (v.y+1.)*height/2., v.z);
            texture_coords[j] = uvw;
            world_coords[j]  = v;
        }
        Vec3f n = (world_coords[2]-world_coords[0])^(world_coords[1]-world_coords[0]);
        n.normalize();
        float intensity = n*light_dir;
        if (intensity>0) {
            triangle(screen_coords[0], screen_coords[1], screen_coords[2], texture_coords[0], texture_coords[1], texture_coords[2], image, zbuffer, *model, intensity);
        }
    }

    image.flip_vertically(); // i want to have the origin at the left bottom corner of the image
    image.write_tga_file("output.tga");
    return 0;
}