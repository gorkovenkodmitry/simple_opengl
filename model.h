#ifndef __MODEL_H__
#define __MODEL_H__

#include <vector>
#include "geometry.h"
#include "tgaimage.h"

class Model {
private:
	std::vector<Vec3f> verts_;
	std::vector<Vec3f> uvw_;
	std::vector<Vec3i> faces_;
	std::vector<Vec3i> textures_;
	TGAImage diffusemap_;
	void load_texture(std::string filename, const char *suffix, TGAImage &img);

public:
	Model(const char *filename);
	~Model();
	int nverts();
	int nfaces();
	int nuvw();
	Vec3f vert(int i);
	Vec3i uvw(int i);
	Vec3i face(int idx);
	Vec3i texture(int idx);
	TGAColor diffuse(Vec2i uv);
};

#endif //__MODEL_H__